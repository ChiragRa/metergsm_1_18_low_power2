

#ifndef MotorControl_H_
#define MotorControl_H_

#include "CommonDefn.h"

// Interrupt details
#define PulsInt 1



// Pin Detials
#define PulsePin 3

#define SQUAREWAVE 4

//Motor Pin 
#define MOTOR_EN 5
#define MOTOR1   6
#define MOTOR2   7

//Set Limit for Valve
#define OPENLIMITPIN 8
#define CLOSELIMITPIN 9

//Motor Direction
#define STP   0
#define FWD 1
#define REV  2

//Valve Control Status
#define OPENED    	2
#define CLOSED    	1
#define NOTCLOSE  0
#define NOT_DEF   	3

// EEPROM Address
#define Pulse_ADR 1 //(4 Bytes)

extern uint8_t OpenLimtSts;
extern uint8_t CloseLimtSts;

extern uint8_t ValCommand;

extern uint32_t Totaliser;
extern volatile uint32_t PulseCounter;
extern volatile uint32_t PrevPulseCounter;
extern volatile uint8_t bToggle;
extern uint8_t ValveSts;

void InitMotor(void);
void MotorControl(uint8_t Dirct);
void PulseToLiter(void);
void ValveControl(void);
uint8_t ValveStatus(void);
void CheckMotor(void);
void Generatesqrwave (void);

#endif