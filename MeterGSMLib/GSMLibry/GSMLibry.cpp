
#include "GSMLibry.h"
#include "MQTTLibry.h"


SoftwareSerial GSMport = SoftwareSerial(A1, A2);

const char SIM_APN[] = {"airtelgprs.com"};
const char SIM_UserName[] = {""};
const char SIM_Pass[] = {""};

char cResponse[200];

uint8_t bModemResp;
uint8_t bRegOnNetwrk;
volatile uint8_t WakeUpFlag;

uint8_t BIP_State;
uint8_t bTCPConnected;

volatile uint8_t TCPConCntr;

const char IP_INITIAL[]    = {"IP INITIAL"};
const char IP_START[]      = {"IP START"};
const char IP_CONFG[]      = {"IP CONFG"};
const char IP_GPRSACT[]	   = {"IP GPRSACT"};
const char IP_STATUS[] 	   = {"IP STATUS"};
const char TCP_CONNG[]     = {"CONNECTING"};
const char IP_CONNECT_OK[] = {"CONNECT OK"};
const char TCP_CLOSG[]     = {"TCP CLOSING"};
const char TCP_CLOSED[]    = {"TCP CLOSED"};
const char IP_PDPDEACT[]   = {"IP PDPDEACT"};


const char AT_ATE0[]     = {"ATE0"};
const char AT_CIPSTART[] = {"AT+CIPSTART=\"TCP\","};
const char AT_CSQ[]      = {"AT+CSQ"};
const char AT_CREG[]     = {"AT+CREG?"};
const char AT_CIPRXGET1[] = {"AT+CIPRXGET=1"};
const char AT_CIPRXGET2[] = {"AT+CIPRXGET=2,100"}; 
const char AT_CIPMUX[]   = {"AT+CIPMUX=0"};
const char AT_CIPMODE[]  = {"AT+CIPMODE?"};
const char AT_CSTT[]     = {"AT+CSTT=\"INTERNET\",\"\",\"\""};
const char AT_CIICR[]    = {"AT+CIICR"};
const char AT_CIFSR[]    = {"AT+CIFSR"};
const char AT_CIPSHUT[]  = {"AT+CIPSHUT"};
const char AT_CIPSTATUS[] = {"AT+CIPSTATUS"};
const char AT_CIPSEND[]  = {"AT+CIPSEND"};
const char AT_CONNECT[]  = {"OK\r\n\r\nCONNECT"};
const char AT_CONNECT_FAIL[] = {"CONNECT_FAIL"};

const char AT_CSCLK_2[] = {"AT+CSCLK=2"};	// Chr 10/8/2020

const char AT_OK[] = {"OK"};
const char AT_ERROR[] = {"ERROR"};

const uint16_t RespTimeOut = 1000;

static void WakeUpFunInt(void);

volatile uint8_t Wake = 0;
volatile uint8_t Low_counter = 0;

uint8_t ConnectCont = 2;	//it sets the limit after which the TCP connection is established

void InitGSM(void)
{
	GSMport.begin(9600);
	delay(100);

//	pinMode(RingPin, INPUT_PULLUP);
	pinMode(RingPin, INPUT);
	
	pinMode(RSTPIN, OUTPUT);



	bModemResp = 0;
	bRegOnNetwrk = 0;
	WakeUpFlag = 0;
	bTCPConnected = 0;
	TCPConCntr = 0;

	
	BIP_State = _IP_STS;
	attachInterrupt(RingInt, WakeUpRing, LOW);		// interrupt assigned for RingPin
}



uint8_t InitTCP(void) 
{
  char TempStr[100];
  static uint8_t RetryModem = 5;		//Retry counter if modem doesnot respond
  static uint8_t RetryrReg = 5;			//Retry counter if the registration of sim is not done
  uint8_t counter = 2;

  static init_prcs_t init_prcs = INIT_COMM;

  switch(init_prcs)
  {

   case INIT_COMM:

  	if(sendATcommand2(AT_ATE0, AT_OK, AT_ERROR, RespTimeOut) == 1)
  	{

          bModemResp = 1;
          Serial.println(F("Modem response"));
	  init_prcs = SIM_REG;
  	}
        else
  	{
          bModemResp = 0;
    	  Serial.println(F("Modem not responsed"));
	  RetryModem--;
  	}

	if((bModemResp == 0) && (RetryModem == 0))
	{
	  init_prcs = GSM_SLEEP;
	}
      break;

  case SIM_REG:

       if(bModemResp == 1)  
       {
    	 sendATcommand2(AT_CSQ, AT_OK, AT_ERROR, RespTimeOut);

       if((sendATcommand2(AT_CREG, "+CREG: 0,1", "+CREG: 0,5", RespTimeOut) == 1) || (sendATcommand2(AT_CREG, "+CREG: 0,1", "+CREG: 0,5", RespTimeOut) == 2))
       {
         bRegOnNetwrk = 1;
	 Serial.println(F("Registeration Done"));      
       }
       else
       {
         bRegOnNetwrk = 0;
	 Serial.println(F("Registeration  NOT Done"));
	 RetryrReg--;
       }

	Serial.println(F("RetryREg =>"));
	Serial.println(RetryrReg);
	
       if((bRegOnNetwrk == 0) && (RetryrReg == 0))
       {
	 Serial.println(F("SLEEPING"));
	 init_prcs = GSM_SLEEP;
       }
       }
      break;

  case GSM_SLEEP:
	sendATcommand2(AT_CSCLK_2, AT_OK, AT_ERROR, RespTimeOut);	
	Sleep();
	if(TCPConCntr >= ConnectCont)
	{
	  TCPConCntr = 0;
	  RetryrReg = 5;
	  Serial.println(F("AGAIN"));
	  init_prcs = INIT_COMM;
	}
	break;	

  default:
     break;

  }

  

    if(bRegOnNetwrk == 1)
    {
      if(BIP_State != _IP_STS)
      {
        BIP_State = TCPStatus();
        Serial.print(F("BIP_State => "));
        Serial.println(BIP_State);
      }

      if(BIP_State != _IP_CONNECT)
      {
        bTCPConnected = 0;
        bMQTTConct = 0;
      }
        
      switch(BIP_State)
      {
        case _IP_STS:
        {
            Serial.println(F("Startint TCP connection"));
  
            SendATCommand(AT_CIPRXGET1, RespTimeOut);

            if(SendATCommand3(AT_CIPMUX, AT_OK, RespTimeOut) == 1)
            {
              SendATCommand3(AT_CIPMODE, AT_OK, RespTimeOut);
              
              BIP_State = _IP_INITIAL;     
            }
            else
              BIP_State = _IP_CLOSE;  

/*
            if(sendATcommand2(AT_CIPMUX, AT_OK, AT_ERROR, RespTimeOut) == 1)
            {
              sendATcommand2(AT_CIPMODE, AT_OK, AT_ERROR, RespTimeOut);
              
              BIP_State = _IP_INITIAL;     
            }
            else
              BIP_State = _IP_CLOSE;  
*/
        }
        break;

        case _IP_INITIAL:
        {
          if(sendATcommand2(AT_CSTT,AT_OK, AT_ERROR, RespTimeOut) == 1)
          {
              
          }
          else
          {
            Serial.println(F("Error setting the APN"));
            BIP_State = _IP_CLOSE;   
          }   
        }
        break;
    
        case _IP_START:
        {
          if(sendATcommand2(AT_CIICR, AT_OK, AT_ERROR, 30000) == 1)
          {
            
          }
          else
          {
            Serial.println(F("Error in bringing up wireless connection"));
            BIP_State = _IP_CLOSE;   
          }
        }
        break;
    
        case _IP_GPRSACT:
        {
          SendATCommand(AT_CIFSR, RespTimeOut);
	
	/*
          if(sendATcommand2(AT_CIFSR, ".", AT_ERROR, 10000) == 1)
          {
            
          }
          else
          {
            Serial.println(F("Error in bringing up wireless connection"));
            BIP_State = _IP_CLOSE;   
          }
*/
                  
        }
        break;

        case _IP_STATUS:
        {
        //  snprintf(TempStr, sizeof(TempStr), "AT+CIPSTART=\"TCP\",\"%s\",\"%s\"", MQTTHost, MQTTPort);

          sprintf(TempStr, "AT+CIPSTART=\"TCP\",\"%s\",\"%s\"", MQTTHost, MQTTPort);

          if(sendATcommand2(TempStr, AT_CONNECT, AT_CONNECT_FAIL, 30000) == 1)
          {
            
          }
          else
          {
            Serial.println(F("Unable to connect to server"));
            BIP_State = _IP_CLOSE;   
          }       
        }
        break;

        case _IP_CONNECT:
        {
          bTCPConnected = 1;  
        }
        break;

        case _IP_CONFIG:
        {
          
                  
        }
        break;

	case _TCP_CLOSED:
	{
		BIP_State = _IP_CLOSE;
	}	
	break;	

        case _IP_CLOSE:
        {
          
        }
        break;
        
        default:
        {
          
        }
      }

      if(BIP_State == _IP_CLOSE)
      {
        CIPSHUT();  
        BIP_State = _IP_STS;
      }
    
  }
  
//  delay(2000);
  
  return(bTCPConnected);
  
}

/*
void ReceiveResponse(uint32_t Timeout) 
{
  char TempChar;
  uint32_t previous;
  uint8_t index = 0;

  char ResStr[100]; 	

  memset(ResStr, '\0', 100);    // Initialize the string

  index = 0;
  previous = millis();
  
  // this loop waits for the answer
  do
  {
    if (GSMport.available() > 0) 
    {
      TempChar = GSMport.read();
      ResStr[index++] = TempChar; 
      Serial.write(TempChar);
    }
  }while ((millis() - previous) < Timeout);

  Serial.print("\r\nReceived response => "); 
  Serial.write(ResStr, 100);    // Send the AT command
  
//  return (ResStr);
}
*/

uint8_t SendATCommand4(const char* ATCmd, uint32_t Timeout) 
{

  uint8_t x = 0;
  uint8_t answer = 0;

  char TempChar;
  uint32_t previous;
  
  memset(cResponse, '\0', 100);    // Initialize the string
  delay(50);

  while(GSMport.available() > 0)
    GSMport.read();   // Clean the input buffer

#ifdef dbg
  Serial.println(ATCmd); 
#endif

  x = 0;
  previous = millis();
  
  GSMport.println(ATCmd); 

  // this loop waits for the answer
  do
  {
    if (GSMport.available() > 0) 
    {
      TempChar = GSMport.read();
      cResponse[x++] = TempChar; 
      Serial.print(TempChar);
     }			
  }while ((millis() - previous) < Timeout);

  Serial.print("\r\nReceived GSM response 2=> "); 
//  Serial.println(cResponse);   
  Serial.write(&cResponse[25], x);   

	
  if (strstr(&cResponse[25], "ON") != NULL)
  {
  	answer = 1;
  }
  else if (strstr(&cResponse[25], "OFF") != NULL)
  {
	answer = 2;
  }	
	
  return (answer);
}

uint8_t SendATCommand3(const char* ATCmd, const char* RespStr, uint32_t Timeout) 
{

  uint8_t x = 0;
  uint8_t answer = 0;

  char TempChar;
  uint32_t previous;
  
  memset(cResponse, '\0', 100);    // Initialize the string
  delay(50);

  while(GSMport.available() > 0)
    GSMport.read();   // Clean the input buffer

#ifdef dbg
  Serial.println(ATCmd); 
#endif

  x = 0;
  previous = millis();
  
  GSMport.println(ATCmd); 

  // this loop waits for the answer
  do
  {
    if (GSMport.available() > 0) 
    {
      TempChar = GSMport.read();
      cResponse[x++] = TempChar; 
      Serial.print(TempChar);
    
	if (strstr(cResponse, RespStr) != NULL)
      	{
            answer = 1;
        }
     }			
  }while ((answer == 0) && (millis() - previous) < Timeout);

  Serial.print("\r\nReceived GSM response 1=> "); 
//  Serial.println(cResponse);   
  Serial.write(cResponse, x);   

  return (answer);
}


char* SendATCommand(const char* ATCmd, uint32_t Timeout) 
{

  uint8_t x = 0,  answer = 0;
  char TempChar;
  uint32_t previous;
  char* str;
  uint8_t index = 0;

  memset(cResponse, '\0', 100);    // Initialize the string
  delay(50);

  while(GSMport.available() > 0)
    GSMport.read();   // Clean the input buffer

#ifdef dbg
  Serial.println(ATCmd); 
#endif

  x = 0;
  previous = millis();
  
  GSMport.print(ATCmd); 
  GSMport.print("\r\n");

  // this loop waits for the answer
  do
  {
    if (GSMport.available() > 0) 
    {
      TempChar = GSMport.read();
      cResponse[x++] = TempChar; 
      Serial.write(TempChar);
    }
  }while ((millis() - previous) < Timeout);

  Serial.print("\r\nReceived GSM response => "); 
 // Serial.println(cResponse);    // Send the AT command
  Serial.write(cResponse, x); 
  
  return (cResponse);
}



int8_t sendATcommand2(char* ATcommand, char* expected_answer1, char* expected_answer2, unsigned int timeout) {

  uint8_t x = 0,  answer = 0;
  char response[100];
  unsigned long previous;

  memset(response, '\0', 100);    // Initialize the string

  delay(50);

  GSMport.flush();
  GSMport.println();
  GSMport.println(ATcommand);    // Send the AT command
  
#ifdef dbg
Serial.println(ATcommand);    // Send the AT command
#endif

  x = 0;
  previous = millis();

  // this loop waits for the answer
  do {
    // if there are data in the UART input buffer, reads it and checks for the asnwer
    if (GSMport.available() != 0) {
      response[x] = GSMport.read();
      x++;
      // check if the desired answer 1  is in the response of the module
      if (strstr(response, expected_answer1) != NULL)
      {
        answer = 1;
        while (Serial.available()) {
          response[x] = GSMport.read();
          x++;
        }
      }
      // check if the desired answer 2 is in the response of the module
      else if (strstr(response, expected_answer2) != NULL)
      {
        answer = 2;
        while (Serial.available()) {
          response[x] = GSMport.read();
          x++;
        }
      }

    }
  }
  // Waits for the asnwer with time out
  while ((answer == 0) && ((millis() - previous) < timeout));
#ifdef dbg
  Serial.println(response);
#endif
  return answer;
}

uint8_t CIPSHUT(void)
{
  char Resp[30];
  char* cResponse = Resp;
  
  cResponse = SendATCommand(AT_CIPSHUT, 2000);

  if(strstr(Resp, "OK") != NULL)
  {
    return 1;  
  }
  else
    return 0;  
}

uint8_t TCPStatus(void)
{
  char* cResponse; 
  uint8_t BIPState;
   
  cResponse = SendATCommand(AT_CIPSTATUS, RespTimeOut);
 
//  Serial.print(F("Received Resp =>"));
//  Serial.println(cResponse);
  
  if(strstr(cResponse, IP_INITIAL) != NULL)
  {
    BIPState = _IP_INITIAL;
    Serial.println(IP_INITIAL);     
  }
  else if(strstr(cResponse, IP_START) != NULL)
  {
    BIPState = _IP_START;
    Serial.println(IP_START);    
  }
  else if(strstr(cResponse, IP_GPRSACT) != NULL)
  {
     BIPState = _IP_GPRSACT;
     Serial.println(IP_GPRSACT);
  }
  else if(strstr(cResponse, IP_STATUS) != NULL)
  {
     BIPState = _IP_STATUS;
     Serial.println(IP_STATUS);
  }
  else if(strstr(cResponse, IP_CONNECT_OK) != NULL)
  {
     BIPState = _IP_CONNECT;
     Serial.println(IP_CONNECT_OK);
  }
  else if(strstr(cResponse, TCP_CLOSED) != NULL)
  {
     BIPState = _TCP_CLOSED;
     Serial.println(TCP_CLOSED);
  } 
  else if(strstr(cResponse, IP_CONFG) != NULL)
  {
     BIPState = _IP_CONFIG;
     Serial.println(IP_CONFG);
  }
  else if(strstr(cResponse, IP_PDPDEACT) != NULL)
  {
     BIPState = _IP_PDPDEACT;
     Serial.println(IP_PDPDEACT);
  }

//  Serial.print(F("BIPState => "));
//  Serial.println(BIPState);
        
  return (BIPState);
}

void RST_GSM (void)
{
   digitalWrite(RSTPIN, LOW);
   DelayFun();
   digitalWrite(RSTPIN, HIGH);
   DelayFun();
   digitalWrite(RSTPIN, LOW);
}

void WakeUpFunInt(void)
{
  sleep_disable();
  detachInterrupt(RingInt);

  WakeUpFlag = 1;
}

void Sleep(void)
{
  Serial.println(F("Enter into Sleep Mode"));

  /* Setup pin2 as an interrupt and attach handler. */
//  attachInterrupt(0, pin2Interrupt, LOW);	//    attachInterrupt(digitalPinToInterrupt(RingPin), WakeUpRing, LOW);     
//  delay(100);
  
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  
  sleep_enable();
  
  sleep_mode();
  
  /* The program will continue from here. */
  
  /* First thing to do is disable sleep. */
  sleep_disable(); 

}

void pin2Interrupt(void)
{
  /* This will bring us back from sleep. */
  
  /* We detach the interrupt to stop it from 
   * continuously firing while the interrupt pin
   * is low.
   */
  detachInterrupt(0);	//detachInterrupt(digitalPinToInterrupt(RingPin));

  Serial.println(F("WAKING_PIN UP"));
 // TCPConCntr = TCPConCntr + 10 + 1;
   
}


void WakeUpRing(void)
{
  sleep_disable();
//  Low_counter++;
//  if(Low_counter > 100)
//  {
  Serial.println(F("WAKING UP"));
//  Low_counter = 0;
//  }
//  detachInterrupt(RingInt);   //detachInterrupt();    
 
  
//  Wake = 1;
}