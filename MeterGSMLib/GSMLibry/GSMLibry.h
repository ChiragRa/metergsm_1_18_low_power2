

#ifndef  GSMLIBRY_H_
#define GSMLIBRY_H_

#include "CommonDefn.h"
#include "TimerLibry.h"

extern SoftwareSerial GSMport;

  //to declare the library


//Define The GSM Pin
#define RingPin  2
#define RSTPIN  12

#define DTR A3		// Chr 10/8/2020

// Ring Interrupt
#define RingInt 0 

extern const char SIM_APN[];
extern const char SIM_UserName[];
extern const char SIM_Pass[];

extern char cResponse[200];

extern uint8_t bModemResp;
extern uint8_t bRegOnNetwrk;
extern volatile uint8_t WakeUpFlag;

extern uint8_t BIP_State;
extern uint8_t bTCPConnected;
extern uint8_t bMQTTConct;
extern volatile uint8_t TCPConCntr;

extern const char AT_CIPRXGET1[];
extern const char AT_CIPRXGET2[]; 

/*
extern const char AT_CIPSTART[];
extern const char AT_CSQ[];
extern const char AT_CREG[];
extern const char AT_CIPMUX[];
extern const char AT_CIPMODE[];
extern const char AT_CSTT[];
extern const char AT_CIICR[];
extern const char AT_CIFSR[];
extern const char AT_CIPSHUT[];
extern const char AT_CIPSTATUS[];
extern const char AT_CIPSEND[];

extern const char AT_CSCLK_2[];
extern const char AT_ATE0[];
*/

extern volatile uint8_t Wake;
extern uint8_t ConnectCont;
typedef enum
{
  INIT_COMM,
  SIM_REG,
  GSM_SLEEP
}init_prcs_t;


#define _IP_STS       (0)
#define _IP_INITIAL   (_IP_STS + 1)
#define _IP_START     (_IP_INITIAL + 1)
#define _IP_CONFIG    (_IP_START + 1)
#define _IP_GPRSACT   (_IP_CONFIG + 1) 
#define _IP_STATUS    (_IP_GPRSACT + 1) 
#define _TCP_CONNG    (_IP_STATUS + 1) 
#define _IP_CONNECT   (_TCP_CONNG + 1)
#define _TCP_CLOSG   (_IP_CONNECT + 1)
#define _TCP_CLOSED   (_TCP_CLOSG + 1)
#define _IP_PDPDEACT  (_TCP_CLOSED + 1)

#define _IP_CLOSE     (_IP_PDPDEACT + 1)

void InitGSM(void);
void RST_GSM (void);

uint8_t InitTCP(void);
char* SendATCommand(const char* ATCmd, uint32_t Timeout);
int8_t sendATcommand2(char* ATcommand, char* expected_answer1, char* expected_answer2, unsigned int timeout);
uint8_t SendATCommand3(const char* ATCmd, const char* RespStr, uint32_t Timeout);
uint8_t SendATCommand4(const char* ATCmd, uint32_t Timeout);
void ReceiveResponse(uint32_t Timeout);
void WakeUpFunInt(void);

void WakeUpRing(void);
void pin2Interrupt(void);

uint8_t CIPSHUT(void);
uint8_t TCPStatus(void);
void Sleep(void);
#endif