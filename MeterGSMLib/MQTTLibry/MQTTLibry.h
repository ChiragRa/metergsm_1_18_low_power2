

#ifndef MQTTLIBRY_H_
#define MQTTLIBRY_H_

#include "CommonDefn.h"
#include "GSMLibry.h"
#include "MotorControl.h"

// Macro defenation
#define _MQTT_CON (0)
#define _MQTT_SUB_01 (_MQTT_CON    + 1)
#define _MQTT_PUB_01 (_MQTT_SUB_01 + 1)
#define _MQTT_SUB_02 (_MQTT_PUB_01 + 1)
#define _MQTT_PUB_02 (_MQTT_SUB_02 + 1)

// CONST declaration
//char MQTTHost[] = {"exploreembedded.com"};
//char MQTTPort[] = {"80"};
/*
extern const char * MQTTHost;
extern const char * MQTTPort;        // checked, also 8883
extern const char * MQTTClientID;
extern const char * MQTTProtocolName;
extern const char * MQTTUsername;
extern const char * MQTTPassword;
extern const char * MQTTTopic;
extern const char * MQTTTopic2;
*/

extern const char MQTTHost[];
extern const char MQTTPort[];        // checked, also 8883
extern const char MQTTClientID[];
extern const char MQTTProtocolName[];
extern const char MQTTUsername[];
extern const char MQTTPassword[];

extern const char MQTTPubTopic_01[];
extern const char MQTTPubTopic_02[];
extern const char MQTTSubTopic_01[];
extern const char MQTTSubTopic_02[];

extern const char MQTTLVL;
extern const char MQTTFlags;
extern const unsigned int MQTTKeepAlive; // testing

extern const char MQTTLVL;
extern const char MQTTFlags;
extern const unsigned int MQTTKeepAlive; // testing
extern const char MQTTQOS;
extern uint8_t MQTTPacketID;


extern char str1[10];
extern unsigned long datalength, checksum, rLength;

extern unsigned char encodedByte;
extern int X;

extern uint8_t bMQTTConct;
extern unsigned short MQTTProtocolNameLength;
extern unsigned short MQTTClientIDLength;
extern unsigned short MQTTUsernameLength;
extern unsigned short MQTTPasswordLength;
extern unsigned short MQTTTopicLength;
extern unsigned short topiclength;
extern unsigned short topiclength2;



void InitMQTT(void);
uint8_t SendMQTTPacket(uint8_t PacketID);
void CreateConPacket(void);
void CreatePubPacket(uint8_t PackID);
void CreateSubPacket(uint8_t PackID);

#endif

