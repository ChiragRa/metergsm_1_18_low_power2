
#include "MQTTLibry.h"


char str1[10] = {"999"};

const char MQTTHost[] = {"io.adafruit.com"};
const char MQTTPort[] = "1883";        // checked, also 8883
const char MQTTClientID[] = "ABCDEF";
const char MQTTProtocolName[] = "MQTT";
const char MQTTPassword[] = "aio_eCqs84PRLBT1fCQOqD1DyezMZDH9";

const char MQTTUsername[] = "AT_METER_002";
const char MQTTPubTopic_01[] = "AT_METER_002/feeds/totalizer2";
const char MQTTPubTopic_02[] = "AT_METER_002/feeds/valvestatus2";

const char MQTTSubTopic_01[] = "AT_METER_002/feeds/valvereqkey2";
const char MQTTSubTopic_02[] = "AT_METER_002/feeds/totreq2";


const char MQTTLVL = 0x03;
const char MQTTFlags = 0xC2;
const unsigned int MQTTKeepAlive = 60; // testing

const char MQTTQOS = 0x00;
uint8_t MQTTPacketID = 0x01;

unsigned long datalength, checksum, rLength;
unsigned char encodedByte;
int X;

uint8_t bMQTTConct;
unsigned short MQTTProtocolNameLength;
unsigned short MQTTClientIDLength;
unsigned short MQTTUsernameLength;
unsigned short MQTTPasswordLength;
unsigned short MQTTTopicLength;
unsigned short topiclength;
unsigned short topiclength2;





void InitMQTT(void)
{
	bMQTTConct = 0;
	MQTTProtocolNameLength = strlen(MQTTProtocolName);
	
}

uint8_t SendMQTTPacket(uint8_t PacketID)
{
  if(sendATcommand2("AT+CIPSEND", ">", "ERROR", 1000)) 
  {
  //  delay(1000);

    if(PacketID == _MQTT_CON)
      CreateConPacket();
    else if(PacketID == _MQTT_SUB_01)
      CreateSubPacket(_MQTT_SUB_01);
    else if(PacketID == _MQTT_SUB_02)
      CreateSubPacket(_MQTT_SUB_02);
    else if(PacketID == _MQTT_PUB_01) 
      CreatePubPacket(_MQTT_PUB_01);  
    else if(PacketID == _MQTT_PUB_02) 
      CreatePubPacket(_MQTT_PUB_02);      
       
        
    GSMport.write(0x1A);

    if (sendATcommand2("", "SEND OK", "SEND FAIL", 2000)) 
    {
      Serial.println(F("Packet sent"));
      return 1;
    }
    else 
    {
      Serial.println(F("Packet not sent"));
      return 0;
    }
  }
}

// first the connect packet is send to establish the connection
void CreateConPacket(void)
{
	Serial.println(F("Sending connect packet"));

  GSMport.write(0x10);
  
  MQTTProtocolNameLength = strlen(MQTTProtocolName);
  MQTTClientIDLength = strlen(MQTTClientID);
  MQTTUsernameLength = strlen(MQTTUsername);
  MQTTPasswordLength = strlen(MQTTPassword);
  datalength = MQTTProtocolNameLength + 2 + 4 + MQTTClientIDLength + 2 + MQTTUsernameLength + 2 + MQTTPasswordLength + 2;
  
  X = datalength;
  do
  {
    encodedByte = X % 128;
    X = X / 128;
      // if there are more data to encode, set the top bit of this byte
    if ( X > 0 ) {
      encodedByte |= 128;
    }

    GSMport.write(encodedByte);
  } while ( X > 0 );
  
  GSMport.write(MQTTProtocolNameLength >> 8);
  GSMport.write(MQTTProtocolNameLength & 0xFF);
  GSMport.write(MQTTProtocolName);

  GSMport.write(MQTTLVL); // LVL
  GSMport.write(MQTTFlags); // Flags
  GSMport.write((uint8_t)(MQTTKeepAlive >> 8));      // write(unsigned int) is ambiguous so byte inorder to determine as integer
  GSMport.write(MQTTKeepAlive & 0xFF);

  GSMport.write(MQTTClientIDLength >> 8);
  GSMport.write(MQTTClientIDLength & 0xFF);
  GSMport.print(MQTTClientID);

  GSMport.write(MQTTUsernameLength >> 8);
  GSMport.write(MQTTUsernameLength & 0xFF);
  GSMport.write(MQTTUsername);

  GSMport.write(MQTTPasswordLength >> 8);
  GSMport.write(MQTTPasswordLength & 0xFF);
  GSMport.write(MQTTPassword);  
}


// the CreatePubPacket  publishes to the server

void CreatePubPacket(uint8_t PackID) //uint8_t *MTopic)
{
  char str[50];

  memset(str, 0, sizeof(str));
  
  if(PackID == _MQTT_PUB_01)	
  {
  	topiclength = strlen(MQTTPubTopic_01);
  	datalength = sprintf((char*)str, "%s%8lu", MQTTPubTopic_01, Totaliser);
  }
  else if(PackID == _MQTT_PUB_02)	
  {
	topiclength = strlen(MQTTPubTopic_02);
	
        if(ValveSts == OPENED)
  	   datalength = sprintf((char*)str, "%s OPENED", MQTTPubTopic_02);
        else if(ValveSts == CLOSED)
           datalength = sprintf((char*)str, "%s CLOSED", MQTTPubTopic_02);

  }	

	Serial.println(F("Sending Publish packet"));	
  Serial.println(str);
    
//  delay(1000);
  GSMport.write(0x30);
  X = datalength + 2;
  
  do
  {
    encodedByte = X % 128;
    
    X = X / 128;
      // if there are more data to encode, set the top bit of this byte
    if ( X > 0 ) 
    {
      encodedByte |= 128;
    }
    GSMport.write(encodedByte);
  }while ( X > 0 );

  GSMport.write(topiclength >> 8);
  GSMport.write(topiclength & 0xFF);
  GSMport.write(str);  
}


// CreateSubPacket() it is used to subscribe the data from the website

void CreateSubPacket(uint8_t PackID)
{
//		Serial.println(F("Sending Subscribe packet"));	

  if(PackID == _MQTT_SUB_01)
   topiclength2 = strlen(MQTTSubTopic_01); 
  else if(PackID == _MQTT_SUB_02)
   topiclength2 = strlen(MQTTSubTopic_02); 
 
  datalength = 2 + 2 + topiclength2 + 1;
  
  GSMport.write(0x82);
  
  X = datalength;
  
  do
  {
    encodedByte = X % 128;
    X = X / 128;
      // if there are more data to encode, set the top bit of this byte
    if ( X > 0 ) 
    {
      encodedByte |= 128;
    }
    
    GSMport.write(encodedByte);
 }while ( X > 0 );
 
  GSMport.write(MQTTPacketID >> 8);        
   
  GSMport.write(MQTTPacketID & 0xFF);
  GSMport.write(topiclength2 >> 8);
  GSMport.write(topiclength2 & 0xFF);
  GSMport.write(MQTTSubTopic_01);
  GSMport.write(MQTTQOS);  
}